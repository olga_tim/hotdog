import React, { Fragment } from 'react';
import Header from './Header';
import ContactsForm from './ContactsForm';

class Contacts extends React.Component {
    render() {
        return (
            <Fragment>
                <Header />
                <ContactsForm />
            </Fragment>

        );
    }
}

export default Contacts;