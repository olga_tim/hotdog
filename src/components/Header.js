import React from 'react';
import { Link } from 'react-router-dom';

class Header extends React.Component {
    getRandomInt = (min, max) => {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    renderHeader = () => {
        const that = this;
        fetch( "https://www.instagram.com/explore/tags/hotdogs/?__a=1", {
            method:'get'
        })
        .then(function(data) {
            return data.json();
        })
        .then(function(response) {
            document.querySelector('.header-img-rgid ul').innerHTML = '';
            for(let i = 0; i < 12; i++) {
                let randomNum = that.getRandomInt(0,response.graphql.hashtag.edge_hashtag_to_media.edges.length);
                const li = document.createElement('li');
                li.style.backgroundImage = "url(" + response.graphql.hashtag.edge_hashtag_to_media.edges[randomNum].node.thumbnail_resources[4].src +")";
                document.querySelector('.header-img-rgid ul').append(li);
            }
        })
    };
    render() {
        return (
            <header className="header">
                <div className="header-top">
                    <img src="../images/Instagram.png" alt="" />
                        <p id="hashtag">#hotdogs</p>
                </div>
                <div className="header-img-rgid">
                    <ul>
                        {this.renderHeader()}
                    </ul>
                </div>
                <div className="header-menu">
                    <nav>
                        <Link to='/'>Menu</Link>
                        <Link to='/'>Catering</Link>
                        <Link to='/'>About us</Link>
                        <Link to='/Contacts'>Contacts</Link>
                    </nav>
                </div>
            </header>
        );
    }
}

export default Header;