import React from 'react';
import { Link } from 'react-router-dom';

class Footer extends React.Component {
    render() {
        return (
            <footer className="footer">
                <div className="footer-left">
                    <p>® 2016 Dirty Dogs all rights reserved</p>
                </div>
                <div className="footer-right">
                    <ul>
                        <li>
                            <p>274 Marconi Blvd. Columbus, Ohio 43215</p>
                        </li>
                        <li>
                            <p>614.538.0095</p>
                        </li>
                        <li>
                            <Link to='#'>Contact Us</Link>
                        </li>
                    </ul>
                </div>
            </footer>
        );
    }
}

export default Footer;