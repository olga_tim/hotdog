import React, { Fragment } from 'react';
import Header from './Header';
import MainContent from './MainContent';
import Footer from './Footer';

class App extends React.Component {

    render() {
        return (
            <Fragment>
                <Header />
                <MainContent />
                <Footer />
            </Fragment>
        );
    }
}

export default App;