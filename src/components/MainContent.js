import React from 'react';
import { Link } from 'react-router-dom';
import { filterByExpiration } from '../helpers';

class MainContent extends React.Component {
    constructor() {
        super();
        this.state = {
            products: [],
        }
    }

    componentDidMount() {
        fetch('https://formula-test-api.herokuapp.com/menu')
            .then(response => {
                return response.json();
            })
            .then(items => filterByExpiration(items))
            .then(products => {
                this.setState({ products });
            });
    }
    renderProducts() {
        return this.state.products.map(product => {
            return (
                <div key={product.id} className="grid-row">
                    <div className="grid-row-col col-text">
                        <h3>{product.name}</h3>
                        <p>{product.description}</p>
                    </div>
                    <div className="grid-row-col">
                        <div className="grid-img" style={ {backgroundImage: "url(" + product.backgroundURL + ")"} }></div>
                    </div>
                </div>
            );
        })
    }
    render() {
        return (
            <main className="main">
                <section className="main-top">
                    <div className="hot-dog-logo"><img src="../images/hotdog.png" alt="" /></div>
                    <h2>Dirty Dogs serves all-beef, vegan and vegatagian hot dogs.</h2>
                    <Link to='#' className="btn">More Dogs ‘n Make Em Hot</Link>
                </section>
                <section className="main-grid">
                    { this.renderProducts() }
                </section>
            </main>
        );
    }
}

export default MainContent;