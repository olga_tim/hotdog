import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import App from './App';
import Contacts from './Contacts';

const Router = () => (
    <BrowserRouter>
    <Switch>
    <Route exact path="/" component={App} />
<Route path="/" component={Contacts} />

</Switch>
</BrowserRouter>
);

export default Router;
