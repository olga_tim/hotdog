import React from 'react';

class Modal extends React.Component {
    render() {
        return (
            <div className="modal">
                <div className="modal-inner">
                    <p>Your message was successfully sent</p>
                </div>
            </div>
        )
    }
}

export default Modal;