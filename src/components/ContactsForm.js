import React, { Fragment } from 'react';
import Modal from './Modal';

class ContactsForm extends React.Component {
    nameRef = React.createRef();
    emailRef = React.createRef();
    commentRef = React.createRef();

    validateEmail = (sEmail) => {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    };

    sendForm = event => {
        let allowSendForm = true;
        event.preventDefault();
        document.querySelectorAll('.contacts-form input, .contacts-form textarea').forEach(function(elem) {
            if(elem.value === "") {
                allowSendForm = false;
                elem.classList.add('error');
            }
            elem.addEventListener('keydown', function(e) {
                if(elem.classList.contains('error')) {
                    elem.classList.remove('error');
                }
            })
        })

        if(this.validateEmail(this.emailRef.current.value) === false) {
            allowSendForm = false;
            this.emailRef.current.className = 'error';
        }
        if(allowSendForm) {
            const formData = {
                name: this.nameRef.current.value,
                email: this.emailRef.current.value,
                comment: this.commentRef.current.value
            };

            fetch( "https://formula-test-api.herokuapp.com/contact", {
                method:'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },

                body: JSON.stringify(formData)
            })
                .then(function(response) {
                    if(response.ok) {
                        document.querySelector('.modal').classList.add('show');

                        setTimeout(function() {
                            document.querySelector('.modal').classList.remove('show');
                        }, 2000)
                    }
                })
        }
    };
    render() {
        return (
            <Fragment>
                <div className="contacts-inner">
                <form className="contacts-form" onSubmit={this.sendForm}>
                    <input name="name" ref={this.nameRef} type="text" placeholder="Name" />
                    <input
                        name="email"
                        ref={this.emailRef}
                        type="text"
                        placeholder="Email"
                    />
                    <textarea name="comment" ref={this.commentRef} placeholder="Comment" />
                    <button type="submit" className="btn">Send</button>
                </form>
                <Modal />
                </div>
            </Fragment>
        );
    }
}

export default ContactsForm;