export function filterByExpiration(items)  {
    const currDay = new Date().getDate();
    const currMonth = new Date().getMonth() + 1;
    const currYear = new Date().getFullYear();
    items = items.filter(function(item) {
        return (+item.expirationDate.split('-')[2] >= currYear && +item.expirationDate.split('-')[0] >= currMonth) && (+item.expirationDate.split('-')[1] > currDay || +item.expirationDate.split('-')[0] > currMonth);
    })
    return items;
};